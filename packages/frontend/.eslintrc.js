module.exports = {
  "parser": "@babel/eslint-parser",
  "env": {
    "browser": true,
    "commonjs": true,
    "es2021": true,
    "node": true
  },
  "extends": [
    "eslint:recommended",
    "plugin:react/recommended"
  ],
  "parserOptions": {
    "ecmaFeatures": {
      "jsx": true
    },
    "ecmaVersion": 12
  },
  "plugins": [
    "react"
  ],
  "settings": {
    "react": {
      // para evitar el cartel "Warning: React version not specified in eslint-plugin-react settings.""
      // https://github.com/yannickcr/eslint-plugin-react#configuration
      "version": "detect"
    }
  },
  "rules": {
    // Prevent React to be marked as unused
    "react/jsx-uses-react": "error",
    // Prevent variables used in JSX to be marked as unused
    "react/jsx-uses-vars": "error",
    "no-unused-vars": 1
  }
};