import React, { useEffect } from "react";
import {
  Link
} from "react-router-dom";

const Home = () => {
  useEffect(() => {
    fetch(process.env.API_URL).then(r => r.json()).then(r => console.log(1, r)).catch(() => null)
    fetch(`${process.env.STRAPI_URL}/qqqs`).then(r => r.json()).then(r => console.log(2, r)).catch(() => null)
  }, [])

  // hero - https://bulma.io/documentation/layout/hero
  // section - https://bulma.io/documentation/layout/section/
  // content - https://bulma.io/documentation/elements/content
  return (
    <React.Fragment>
      <section className="hero is-medium is-link">
        <div className="hero-body">
          <p className="title">
            Medium hero
          </p>
          <p className="subtitle">
            Medium subtitle
          </p>
        </div>
      </section>
      <section className="section is-small">
        {/*<!-- This content class can be used in any context where you just want to write some text. -->*/}
        <div className="content is-normal">
          <h2>Hola</h2>
          <p>
            Esta es una aplicación para probar qué tan potente es el armado de interfaces tipo mobile
            con <a href="https://material-ui.com/" rel="noopener noreferrer nofollow" target="_blank">Material-UI</a>
          </p>
          <p>
            La mayoría de los componentes de esta página son cargados dinámicamente, asegurando
            la optimización de la velocidad de carga del contenido de la app.
          </p>
          <p>
            Está app se construyó
            usando <a href="https://webpack.js.org/" rel="noopener noreferrer nofollow" target="_blank">Webpack</a>,   y
            con <a href="https://reactjs.org/" rel="noopener noreferrer nofollow" target="_blank">React</a> como
            framework base.
          </p>
          <p>
            El buen funcionamiento en todo tipo de navegadores está asegurado gracias
            a <a href="https://babeljs.io/" rel="noopener noreferrer nofollow" target="_blank">Babel</a>.
          </p>
          <p>
            El código fuente de la app se puede encontrar
            en <a href="https://gitlab.com/saius/react-webpack-example/-/tree/material" rel="noopener noreferrer nofollow" target="_blank">saius/react-webpack-example</a>.
          </p>
          <p>
            Se puede ver un reporte de las librerías usadas (y cómo están empaquetadas
            por webpack)
            en <a href="report.html" rel="noopener noreferrer nofollow" target="_blank">report.html</a>.
          </p>
          <a className="button is-primary">Primary</a>
          <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
          <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
        </div>
      </section>
    </React.Fragment>
  )
}
export default Home;
