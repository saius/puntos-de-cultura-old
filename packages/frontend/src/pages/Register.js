import React from "react";
import styled from "styled-components";

const Form = styled.form`
  max-width: 500px;
  margin: 0 auto;
`;

const Register = () => {
  const [username, setUsername] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [policy, setPolicy] = React.useState(false);

  // https://reactgo.com/react-hooks-forms/
  function usernameOnChange(e) {
    setUsername(e.target.value)
  }
  function emailOnChange(e) {
    setEmail(e.target.value)
  }
  function passwordOnChange(e) {
    setPassword(e.target.value)
  }
  function policyOnChange(e) {
    setPolicy(e.target.value)
  }

  const [usernameError, setUsernameError] = React.useState('');
  const [emailError, setEmailError] = React.useState('');
  const [passwordError, setPasswordError] = React.useState('');
  const [policyError, setPolicyError] = React.useState('');

  function crearCuentaClick(e){
    e.preventDefault()

    if (!username) setUsernameError('Este campo es requerido')
    else setUsernameError('')

    if (!email) setEmailError('Este campo es requerido')
    else setEmailError('')

    if (!password) setPasswordError('Este campo es requerido')
    else setPasswordError('')

    if (!policy) setPolicyError('Debe aceptar los términos y condiciones para crearse una cuenta')
    else setPolicyError('')
  }

  // form - https://bulma.io/documentation/form/general
  // container - https://bulma.io/documentation/layout/container/
  return (
    <section className="section">
      <div className="container">
        <Form className="box">
          <h1 className="title is-4">Registro</h1>

          <div className="field">
            <label className="label">Nombre de usuario</label>
            <div className={`control has-icons-left ${usernameError ? "has-icons-right" : ""}`}>
              <input
                className={`input ${usernameError ? "is-danger" : ""}`}
                type="text"
                placeholder=""
                value={username}
                onChange={usernameOnChange}
              />
              <span className="icon is-small is-left">
                <i className="fa fa-user"></i>
              </span>
              { usernameError &&
                <span className="icon is-small is-right">
                  <i className="fa fa-exclamation-triangle"></i>
                </span>
              }
            </div>
            { usernameError &&
              <p className="help is-danger">{usernameError}</p>
            }
          </div>

          <div className="field">
            <label className="label">Email</label>
            <div className={`control has-icons-left ${emailError ? "has-icons-right" : ""}`}>
              <input
                className={`input ${emailError ? "is-danger" : ""}`}
                type="email"
                placeholder=""
                value={email}
                onChange={emailOnChange}
              />
              <span className="icon is-small is-left">
                <i className="fa fa-envelope"></i>
              </span>
              { emailError &&
                <span className="icon is-small is-right">
                  <i className="fa fa-exclamation-triangle"></i>
                </span>
              }
            </div>
            { emailError &&
              <p className="help is-danger">{emailError}</p>
            }
          </div>

          <div className="field">
            <label className="label">Contraseña</label>
            <div className={`control has-icons-left ${passwordError ? "has-icons-right" : ""}`}>
              <input
                className={`input ${passwordError ? "is-danger" : ""}`}
                type="password"
                value={password}
                onChange={passwordOnChange}
                placeholder="" />
              <span className="icon is-small is-left">
                <i className="fa fa-lock"></i>
              </span>
              { passwordError &&
                <span className="icon is-small is-right">
                  <i className="fa fa-exclamation-triangle"></i>
                </span>
              }
            </div>
            { passwordError &&
              <p className="help is-danger">{passwordError}</p>
            }
          </div>

          <div className="field py-1">
            <div className="control">
              <label className="checkbox">
                <input
                  type="checkbox"
                  value={policy}
                  onChange={policyOnChange} /> Acepto los{" "}
                <a href="#">términos y condiciones</a>
              </label>
            </div>
            { policyError &&
              <p className="help is-danger">{policyError}</p>
            }
          </div>

          <div className="field">
            <div className="control">
              <button onClick={crearCuentaClick} className="button is-link is-fullwidth">
                Crear cuenta
              </button>
            </div>
          </div>
        </Form>
      </div>
    </section>
  );
};

export default Register;
