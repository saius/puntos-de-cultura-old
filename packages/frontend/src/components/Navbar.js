import React from "react";
import {Link} from "react-router-dom";

const Navbar = () => {
  const [isActive, setisActive] = React.useState(false);

  function toggleBurgerMenu() {
    setisActive(!isActive);
  }

  // navbar - https://bulma.io/documentation/components/navbar/#basic-navbar
  // burger react - https://stackoverflow.com/questions/55015841/burger-menu-using-bulma-on-react-not-working
  // sass variables - https://bulma.io/documentation/components/navbar/#variables
  return (
    <nav className="navbar" role="navigation" aria-label="main navigation">
      {/*<!-- The navbar brand is always visible, is the left side of the navbar. -->*/}
      <div className="navbar-brand">
        <a className="navbar-item" href="https://bulma.io">
          <img
            src="https://bulma.io/images/bulma-logo.png"
            width="112"
            height="28"
          />
        </a>

        <a
          onClick={() => toggleBurgerMenu()}
          role="button"
          className={`navbar-burger ${isActive ? "is-active" : ""}`}
          aria-label="menu"
          aria-expanded="false"
          data-target="navbarBasicExample"
        >
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </a>
      </div>

      {/*<!-- The navbar-menu is hidden on touch devices, is the counterpart of the navbar brand -->*/}
      <div
        id="navbarBasicExample"
        className={`navbar-menu ${isActive ? "is-active" : ""}`}
      >
        {/*<!-- navbar-start will appear on the left -->*/}
        <div className="navbar-start">
          <Link
            className="navbar-item"
            to={`/`}
            onClick={() => toggleBurgerMenu()}
          >
            Home
          </Link>
          <Link
            className="navbar-item"
            to={`/topics`}
            onClick={() => toggleBurgerMenu()}
          >
            Topics
          </Link>
          <Link
            className="navbar-item"
            to={`/users`}
            onClick={() => toggleBurgerMenu()}
          >
            Users
          </Link>

          <div className="navbar-item has-dropdown is-hoverable">
            <a className="navbar-link">More</a>

            <div className="navbar-dropdown">
              <a className="navbar-item">About</a>
              <a className="navbar-item">Jobs</a>
              <a className="navbar-item">Contact</a>
              <hr className="navbar-divider" />
              <a className="navbar-item">Report an issue</a>
            </div>
          </div>
        </div>

        {/*<!-- navbar-end will appear on the right -->*/}
        <div className="navbar-end">
          <div className="navbar-item">
            <div className="buttons">
              <Link to='/registro' className="button is-primary">
                <strong>Registro</strong>
              </Link>
              <Link to='/ingreso' className="button is-light">Ingresar</Link>
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
