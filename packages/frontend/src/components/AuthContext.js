import React, {createContext, useState, useEffect} from "react";
import PropTypes from 'prop-types';

const AuthContext = createContext();

const AuthProvider = ({children}) => {
  // el primer state antes de recuperar la sesión es null
  const [loggedIn, setLoggedIn] = useState(null);

  const authApiPath = `${process.env.API_URL}/user`

  const authFetch = (url, options) =>
    fetch(url, Object.assign(options || {}, {credentials: "include"})).then(
      res => {
        if (res.status == 401){
          setLoggedIn(false)
          throw new Error('Usuarix no autenticadx')
        }else if (res.status == 403){
          throw new Error('Operación no permitida')
        }
        return res
      }
    )

  const restoreSession = () => authFetch(`${authApiPath}/restore-session`)
    .then(() => setLoggedIn(true))
    .catch(() => setLoggedIn(false))

  const login = (username, password) => authFetch(`${authApiPath}/login`, {
    method: "POST",
    body: JSON.stringify({ username, password }),
    headers: {
        'Content-Type': 'application/json'
    }
  }).then(res => setLoggedIn(res.status == 200))

  const logout = () => authFetch(`${authApiPath}/logout`)
    .then(() => setLoggedIn(false))

  useEffect(restoreSession, [])

  return (
    <AuthContext.Provider value={{loggedIn, authFetch, login, logout}}>
      {children}
    </AuthContext.Provider>
  );
};

AuthProvider.propTypes = {
  children: PropTypes.object
};

// para usar en componentes (no hooks)
const withAuth = Child => {
  const withAuthComponent = (props) => (
    <AuthContext.Consumer>
      {authContext => <Child {...props} authContext={authContext} />}
      {/*alternative: context => <Child {...props} {...context} />*/}
    </AuthContext.Consumer>
  );
  withAuthComponent.displayName = 'withAuthComponent'
  return withAuthComponent
}

export { AuthContext, AuthProvider, withAuth }
