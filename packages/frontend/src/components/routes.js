import React from "react";
import Topics from "./Topics";
import Users from "./Users";

const routes = [
  {
    path: "/",
    component: Home
  },
  {
    path: "/about",
    component: About
  },
  {
    path: "/users",
    component: Users
  },
  {
    path: "/topics",
    component: Topics
  }
];

function Home() {
  return <h2>Home</h2>;
}

function About() {
  return <h2>About</h2>;
}

export default routes;
