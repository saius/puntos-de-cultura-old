import React from "react";
import {Switch, Route} from "react-router-dom";
import Home from "pages/Home";
import NotFound from "pages/NotFound";
import Topics from "pages/Topics";
import Users from "pages/Users";
import Login from "pages/Login";
import Register from "pages/Register";

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/topics" component={Topics} />
      <Route path="/users" component={Users} />
      <Route path="/ingreso" component={Login} />
      <Route path="/registro" component={Register} />
      <Route component={NotFound} />
    </Switch>
  );
};

export default Routes;
