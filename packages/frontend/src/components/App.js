import React from "react";
import {BrowserRouter as Router} from "react-router-dom";
//import {AuthProvider} from 'components/AuthContext'
import Routes from "./Routes";
import Navbar from "components/Navbar";
import Footer from "components/Footer";

import 'styles/main.scss'

const App = () => {
  return (
    <React.StrictMode>
      {/*<AuthProvider>*/}
        <Router>
          <Navbar />
          <Routes />
          <Footer />
        </Router>
      {/*</AuthProvider>*/}
    </React.StrictMode>
  );
};

export default App;
