// express no ataja los errores cuando se le pasa una función async como middleware

// https://stackoverflow.com/questions/50047307/catching-all-errors-in-async-await-express-route-handlers
// https://madole.xyz/error-handling-in-express-with-async-await-routes
function asyncRoute(middleware) {
  function wrappedMiddleware(req, res, next) {
    return Promise.resolve(middleware(req, res, next)).catch(next);
  }
  return wrappedMiddleware;
}

module.exports = asyncRoute;
