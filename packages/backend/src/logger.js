const winston = require("winston");
const config = require("./config");

// uso - https://github.com/winstonjs/winston#usage
// ejemplos - https://github.com/winstonjs/winston/blob/master/examples/quick-start.js
const logger = winston.createLogger({
  level: "info",
  format: winston.format.combine(
    winston.format.timestamp({
      format: "YYYY-MM-DD HH:mm:ss"
    }),
    config.DEV_MODE ? winston.format.colorize() : winston.format.uncolorize(),
    // para admitir string interpolation ("%s")
    winston.format.splat(),
    winston.format.printf(
      ({level, message, timestamp}) => `[${timestamp}] ${level}: ${message}`
    )
  ),
  defaultMeta: {service: "backend"},
  transports: [new winston.transports.Console()]
});

module.exports = logger;
