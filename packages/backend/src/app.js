const express = require("express");
const morgan = require("morgan");
const compression = require("compression");
const session = require("express-session");
const MySQLStore = require("express-mysql-session")(session);

const config = require("./config");
const db = require("./db");
const log = require("./logger");
const securitize = require("./middleware/securitize");
const passports = require("./middleware/passports");

const userRoute = require("./routes/user");

const app = express();

log.info(`Creando app en modo=${config.NODE_ENV}`);

// loggear requests
app.use(morgan(config.DEV_MODE ? "dev" : "combined"));

// unless trust proxy is configured it will incorrectly register the proxy’s IP address as the client IP address
// enabling it uses X-Forwarded-* headers: https://expressjs.com/en/guide/behind-proxies.html
//app.set('trust proxy', <TRUE/IP/SUBNET>)

// cors, csp, ratelimiter, etc
securitize(app);

// comprimir las responses comprimibles
app.use(compression());

// parsear json
app.use(express.json());

// parsear POST (application/x-www-form-urlencoded)
app.use(
  express.urlencoded({
    extended: true
  })
);

// cookies para passport

// https://github.com/chill117/express-mysql-session#usage
// crea una tabla 'sessions'
var sessionStore = new MySQLStore({
  host: config.DB_HOST,
  port: 3306,
  user: config.DB_USER,
  password: config.DB_PASS,
  database: config.DB_NAME
});

app.use(
  session({
    key: "cookie_de_login",
    secret: config.SESSION_SECRET,
    store: sessionStore,
    resave: false,
    saveUninitialized: false
  })
);

// uso en memoria/sin db
/*app.use(
  session({
    key: "cookie_de_login",
    secret: config.SESSION_SECRET,
    resave: false,
    saveUninitialized: false
  })
);*/

// seteamos logins (local, google, etc.)
passports(app);

// rutas de nuestra app
app.use("/user", userRoute);
app.use("/", (req, res) => res.json({msg: "hola!"}));

// custom error handler - https://expressjs.com/en/guide/error-handling.html
// para llamar con next(err) o Promise.catch(next)
app.use(function(err, req, res, next) {
  log.error(`Ha surgido un error en la aplicación:\n${err.stack || err}`);

  if (res.headersSent) {
    return next(err);
  } else {
    let status = err.status || 500;
    res.status(status);
    if (config.DEV_MODE) res.send(err.message);
    else res.send("Ha ocurrido un error");
  }
});

// arrancamos el server! si no anda la db lo arrancamos igual (finally)
db.init()
  .catch(err => log.error(`Error al iniciar la DB:\n${err.stack || err}`))
  .finally(() => {
    if (config.SERVER_ENABLE_HTTPS) {
      // For production systems, you're probably better off using Nginx or HAProxy
      // to proxy requests to your nodejs app (as HTTP internally)
      const fs = require("fs");
      const https = require("https");
      const privateKey = fs.readFileSync(config.HTTPS_KEY_PATH, "utf8");
      const certificate = fs.readFileSync(config.HTTPS_CERT_PATH, "utf8");
      const options = {key: privateKey, cert: certificate};
      https.createServer(options, app).listen(config.SERVER_PORT, () => {
        log.info(
          `App HTTPS escuchando conexiones en https://localhost:${config.SERVER_PORT}`
        );
      });
    } else
      app.listen(config.SERVER_PORT, () => {
        log.info(
          `App escuchando conexiones en http://localhost:${config.SERVER_PORT}`
        );
      });
  });
