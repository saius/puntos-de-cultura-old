// sequelize - https://sequelize.org/master/
const {Sequelize, Model, DataTypes} = require("sequelize");
const bcrypt = require("bcrypt");

const config = require("../config");
const log = require("../logger");
const User = require("./user");

const modelsList = [User];

// objeto a exportar para usar en el código
const db = {};

// basado en https://github.com/IHIutch/heroku-nuxt/blob/ea7dcc94371620e502b5d7586a2c409d5babf5ca/api/v1/models/index.js
db.init = async () => {
  // configuración de conexión - https://sequelize.org/master/manual/getting-started.html
  // para hacerlo en memoria
  //const sequelize = new Sequelize('sqlite::memory:');
  // para hacerlo vía sqlite en un archivo
  /*const sequelize = new Sequelize({
    dialect: "sqlite",
    storage: config.DB_FILENAME,
    logging: config.DB_DEBUG_SQL && console.log
  });*/
  const sequelize = new Sequelize(
    config.DB_NAME,
    config.DB_USER,
    config.DB_PASS,
    {
      host: config.DB_HOST,
      dialect: "mysql",
      logging: config.DB_DEBUG_SQL && (msg => log.info(`SQL> ${msg}`))
    }
  );
  //log.info(config.DB_NAME, config.DB_USER, config.DB_PASS, config.DB_HOST);

  db.sequelize = sequelize;

  // inicializamos modelos
  const models = await Promise.all(
    modelsList.map(model => model.init(sequelize, Sequelize.DataTypes))
  );

  // hacemos asociaciones (si existen) y cargamos modelos en objeto db
  models.forEach(model => {
    if (model.associate) model.associate(db);
    db[model.name] = model;
  });

  // probamos conexión
  // https://sequelize.org/master/class/lib/sequelize.js~Sequelize.html#instance-method-authenticate
  await sequelize.authenticate();
  log.info("Conexión a base de datos exitosa");

  // crear las tablas si no existen
  // https://sequelize.org/master/manual/model-basics.html#model-synchronization
  await sequelize.sync();

  if (config.DEV_MODE && config.TEST_USERNAME) {
    // si no hay ningún usuarie cargado cargar el de testeo
    const users = await db.User.count();
    if (!users) {
      const testUser = await db.User.registerUser({
        username: config.TEST_USERNAME,
        email: config.TEST_EMAIL,
        password: config.TEST_PASSWORD
      });

      log.info(
        `Usuarix de testeo creadx: ${config.TEST_USERNAME}/${config.TEST_PASSWORD}`
      );
    }
  }
};

module.exports = db;
