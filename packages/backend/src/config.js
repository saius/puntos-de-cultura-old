const dotenv = require("dotenv");

// cargamos .env
dotenv.config();

parseFlag = value =>
  value && ["true", "y", "yes", "1"].includes(value.toLowerCase());

const config = {
  NODE_ENV: process.env.NODE_ENV,
  DEV_MODE: process.env.NODE_ENV == "development",

  SERVER_PORT: parseInt(process.env.SERVER_PORT) || 3000,

  SERVER_ENABLE_HTTPS: parseFlag(process.env.SERVER_ENABLE_HTTPS) || false,
  HTTPS_KEY_PATH: process.env.HTTPS_KEY_PATH,
  HTTPS_CERT_PATH: process.env.HTTPS_CERT_PATH,

  // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/upgrade-insecure-requests
  HEADER_CSP_UPGRADE_INSECURE:
    parseFlag(process.env.HEADER_CSP_UPGRADE_INSECURE) || false,
  HEADER_CORS_ORIGIN_URL: process.env.HEADER_CORS_ORIGIN_URL,

  SESSION_SECRET: process.env.SESSION_SECRET,

  //DB_FILENAME: process.env.DB_FILENAME || "database.sqlite",
  DB_DEBUG_SQL: parseFlag(process.env.DB_DEBUG_SQL) || false,
  DB_NAME: process.env.DB_NAME || "backend",
  DB_USER: process.env.DB_USER || "backend",
  DB_PASS: process.env.DB_PASS || "backend",
  DB_HOST: process.env.DB_HOST || "localhost",

  PASSWORDS_BCRYPT_SALT_ROUNDS:
    parseInt(process.env.PASSWORDS_BCRYPT_SALT_ROUNDS) || 12,

  TEST_USERNAME: process.env.TEST_USERNAME || "usu",
  TEST_EMAIL: process.env.TEST_EMAIL || "asd@asd.asd",
  TEST_PASSWORD: process.env.TEST_PASSWORD || "123",

  MAILER_FROM_ADDRESS: process.env.MAILER_FROM_ADDRESS,
  MAILER_HOST: process.env.MAILER_HOST || "localhost",
  MAILER_PORT: parseInt(process.env.MAILER_PORT) || 25,
  MAILER_SECURE: parseFlag(process.env.MAILER_SECURE) || false,
  MAILER_REQUIRE_TLS: parseFlag(process.env.MAILER_REQUIRE_TLS) || false,
  MAILER_AUTH_USER: process.env.MAILER_AUTH_USER,
  MAILER_AUTH_PASS: process.env.MAILER_AUTH_PASS,
  MAILER_SELF_SIGNED_CERT:
    parseFlag(process.env.MAILER_SELF_SIGNED_CERT) || false,
  MAILER_DEBUG: parseFlag(process.env.MAILER_DEBUG) || false,

  GOOGLE_CLIENT_ID: process.env.GOOGLE_CLIENT_ID,
  GOOGLE_CLIENT_SECRET: process.env.GOOGLE_CLIENT_SECRET
};

if (
  config.SERVER_ENABLE_HTTPS &&
  !(config.HTTPS_KEY_PATH && config.HTTPS_CERT_PATH)
) {
  console.error(
    "Debe configurar HTTPS_KEY_PATH y HTTPS_CERT_PATH para habilitar HTTPS. Deshabilitando HTTPS."
  );
  config.SERVER_ENABLE_HTTPS = false;
}

module.exports = config;
