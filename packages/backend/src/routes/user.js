const express = require("express");

const {restrict} = require("../auth");
const db = require("../db");
const asyncRoute = require("../asyncRoute");

const router = express.Router();

// las rutas de registro, login, etc. están en src/middleware/passports/local-login.js

router.get(
  "/",
  asyncRoute(async (req, res, next) => {
    let users = await db.User.findAll();
    res.json(users.map(user => user.username));
  })
);

router.get("/private", restrict, async (req, res) => {
  res.send(req.user);
});

module.exports = router;
