const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const {promisify} = require("util");
const {body, validationResult} = require("express-validator");

const db = require("../../db");
const {restrict} = require("../../auth");
const asyncRoute = require("../../asyncRoute");
const log = require("../../logger");

module.exports = app => {
  passport.use(
    new LocalStrategy(async (username, password, done) => {
      let user;
      try {
        user = await db.User.localLoginUser(username, password);
      } catch (err) {
        return done(err);
      }
      if (user) return done(null, user);
      else return done(null, false, {message: "Login inválido"});
    })
  );

  // express-validator - https://express-validator.github.io/docs/index.html
  // validators - https://github.com/validatorjs/validator.js#validators
  // sanitizers - https://github.com/validatorjs/validator.js#sanitizers
  app.post(
    "/register",
    body("username")
      .not()
      .isEmpty()
      .withMessage("Nombre de usuario requerido")
      .isLength({max: 100})
      .withMessage("Nombre de usuario muy largo")
      .isAscii()
      .withMessage("Nombre de usuario con caracteres inválidos")
      // sanitizadores
      .escape()
      .stripLow()
      .trim(),
    body("email")
      .not()
      .isEmpty()
      .withMessage("Email requerido")
      .isLength({max: 100})
      .withMessage("Email muy largo")
      .isEmail()
      .withMessage("Email inválido")
      .isAscii()
      .withMessage("Email con caracteres inválidos")
      // sanitizadores
      .normalizeEmail()
      .escape()
      .stripLow()
      .trim(),
    body("password")
      .not()
      .isEmpty()
      .withMessage("Contraseña requerida")
      .isLength({min: 6})
      .withMessage("Contraseña muy corta")
      .isLength({max: 100})
      .withMessage("Contraseña muy larga")
      .isAscii()
      .withMessage("Contraseña con caracteres inválidos")
      .stripLow()
      .trim(),
    asyncRoute(async (req, res) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res
          .status(400)
          .json({errors: errors.array().map(err => err.msg)});
      }

      const {username, email, password} = req.body;

      const existingUser = await db.User.getByEmail(email);

      if (existingUser) {
        return res
          .status(400)
          .json({errors: ["Ya existe un usuario con ese email"]});
      }

      const newUser = await db.User.registerUser({username, email, password});
      log.info("Nuevo usuario registrado: %s", username);

      res.send();
    })
  );

  app.post("/login", passport.authenticate("local"), (req, res) => {
    // When authentication was successful this function get called.
    // `req.user` contains the authenticated user.
    res.sendStatus(req.user ? 200 : 404);
  });

  app.get(
    "/logout",
    restrict,
    asyncRoute(async (req, res, next) => {
      if (req.session) {
        // For Delete Session Object
        const adestroy = promisify(req.session.destroy).bind(req.session);
        await adestroy();
      }
      res.sendStatus(200);
    })
  );

  app.get("/restore-session", restrict, (req, res) => res.sendStatus(200));
};
