const path = require("path");
const nodeMailer = require("nodemailer");
// https://github.com/yads/nodemailer-express-handlebars
const nodeMailerHbs = require("nodemailer-express-handlebars");
const config = require("./config");
const log = require("./logger");

// se puede usar namshi/smtp para pruebas locales https://hub.docker.com/r/namshi/smtp
// levantar una instancia local con el comando: docker run -it -p25:25 namshi/smtp
// otro servicio pero en línea para esto es mailtrap.io
// y otra opción para tanto recibir como enviar es docker-mailserver https://hub.docker.com/r/tvial/docker-mailserver

const transportOptions = {
  host: config.MAILER_HOST,
  port: config.MAILER_PORT,
  secure: config.MAILER_SECURE,
  requireTLS: config.MAILER_REQUIRE_TLS,
  auth: {
    user: config.MAILER_AUTH_USER,
    pass: config.MAILER_AUTH_PASS
  }
};

const transporter = nodeMailer.createTransport(transportOptions);

transporter.use(
  "compile",
  nodeMailerHbs({
    // las opciones que pueden ir en viewEngine son:
    // https://github.com/ericf/express-handlebars#configuration-and-defaults
    viewEngine: {
      layoutsDir: path.join(__dirname, "mail-templates")
    },
    viewPath: path.join(__dirname, "mail-templates")
  })
);

// esto es para smtp/imap local con certificados autofirmados
// esquiva el error "Error: self signed certificate"
if (config.DEV_MODE && config.MAILER_SELF_SIGNED_CERT)
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const sendMail = options => {
  // options = {from, to, subject, text, html, template, variables/context}
  options.from = config.MAILER_FROM_ADDRESS;
  if (options.variables) {
    options.context = options.variables;
    delete options.variables;
  }
  //log.info("%s", options);

  log.info("Enviando mail...");
  const promise = new Promise((res, rej) =>
    transporter.sendMail(options, (error, data) => {
      if (error) rej(error);
      else res(data);
    })
  );

  return promise
    .then(data => {
      log.info("Mail enviado a cola");
      config.MAILER_DEBUG && log.info("%s", data);
      return data;
    })
    .catch(error => {
      log.error("Error al mandar mail");
      config.MAILER_DEBUG && log.error(error);
      throw error;
    });
};

/*sendMail({
  to: config.MAILER_FROM_ADDRESS,
  subject: "Prueba",
  template: "registro",
  variables: {
    name: "Pepa"
  }
});*/

module.exports = {sendMail};
