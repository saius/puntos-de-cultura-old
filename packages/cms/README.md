Aplicación de Strapi creada a partir de [strapi/strapi](https://github.com/strapi/strapi-docker)
en su última versión el 5 de marzo de 2021.

Se descargó la imagen oficial de Strapi y se congeló en este código.

### Cómo se creó el código

Primero se levantó el servicio utilizando
[las recomendaciones oficiales](https://strapi.io/documentation/developer-docs/latest/setup-deployment-guides/installation/docker.html).

Después se instaló el plugin CKEditor para tener un mejor editor de texto siguiendo
[la guía oficial](https://strapi.io/documentation/developer-docs/latest/guides/registering-a-field-in-admin.html).
Se crearon archivos de automatización bajo la carpeta `plugin-editor-wysiwyg` de este respositorio.

Posteriormente se creo el `Dockerfile` como indica
[la guía oficial](https://github.com/strapi/strapi-docker#how-to-use-strapibase)
, haciendo las modificaciones pertinentes.
