# https://strapi.io/documentation/developer-docs/latest/guides/registering-a-field-in-admin.html#introduction

# docker exec -it STRAPI_CONTAINER bash
# strapi generate:plugin wysiwyg
# cd plugins/wysiwyg
# npm install @ckeditor/ckeditor5-react @ckeditor/ckeditor5-build-classic

APP_PATH=../app

sudo mkdir -p $APP_PATH/plugins/wysiwyg/admin/src/components/MediaLib/
sudo cp MediaLib-index.js $APP_PATH/plugins/wysiwyg/admin/src/components/MediaLib/index.js

sudo mkdir -p $APP_PATH/plugins/wysiwyg/admin/src/components/Wysiwyg/
sudo cp Wysiwyg-index.js $APP_PATH/plugins/wysiwyg/admin/src/components/Wysiwyg/index.js

sudo mkdir -p $APP_PATH/plugins/wysiwyg/admin/src/components/CKEditor/
sudo cp CKEditor-index.js $APP_PATH/plugins/wysiwyg/admin/src/components/CKEditor/index.js

sudo cp admin-index.js $APP_PATH/plugins/wysiwyg/admin/src/index.js

# docker exec -it STRAPI_CONTAINER bash
# strapi build

