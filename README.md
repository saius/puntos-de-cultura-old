Este repositorio en un __monorepo__.

Dentro de `packages` se pueden ver los proyectos que lo conforman.

Se utiliza [Lerna](https://lerna.js.org/) para administrar los paquetes.

`npx lerna bootstrap`

`npx lerna version`

Para levantar entorno de desarrollo localmente:
```
docker-compose -f docker/docker-compose.yml up mysql strapi
npm run serve --prefix packages/backend
npm run build --prefix packages/frontend
```

Para levantar entorno de producción localmente:

`docker-compose -f docker/docker-compose.yml up`
