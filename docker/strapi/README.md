[Strapi Docker](https://github.com/strapi/strapi-docker)

Recomendaciones oficiales para levantar con docker:   
https://strapi.io/documentation/developer-docs/latest/setup-deployment-guides/installation/docker.html

Guía oficial para instalar el plugin CKEditor para tener un mejor editor de texto siguiendo:   
https://strapi.io/documentation/developer-docs/latest/guides/registering-a-field-in-admin.html

Esto último se automatizó con los archivos de esta carpeta.

Más ejemplo de dockers con strapi:   
https://github.com/strapi/strapi-docker/tree/master/examples

### Corriendo local

Buildeamos imagen personalizada:

`docker build -t pdc-strapi .`

Corremos container definiendo un volumen para hacer la data persistente y exponiendo el puerto 1337:

`docker run -it -p 1337:1337 -v `pwd`/docker-volume-data:/srv/app pdc-strapi`

Por defecto va a guardar los datos en un archivo SQLite en el volumen dado.

Si queremos que se conecte a una bse MySql debemos levantarla localmente (o con docker), 
hacer su puerto públicamente accesible y correr strapi:

```
docker run -it \
  -e DATABASE_CLIENT=mysql \
  -e DATABASE_NAME=strapi \
  -e DATABASE_HOST=localhost \
  -e DATABASE_PORT=3306 \
  -e DATABASE_USERNAME=strapi \
  -e DATABASE_PASSWORD=strapi \
  -p 1337:1337 \
  -v `pwd`/docker-volume-data:/srv/app \
  pdc-strapi
```

como indica https://github.com/strapi/strapi-docker#how-to-use-strapistrapi
