#!/bin/sh

# corremos el script original pasándole como primer argumento "strapi" así instala todo
# https://github.com/strapi/strapi-docker/blob/master/strapi/docker-entrypoint.sh
docker-entrypoint.sh strapi -v

if [ ! -d "/srv/app/plugins/wysiwyg" ]; then
echo "Installing plugin..."

PLUGIN_FILES_PATH=/srv/plugin-editor-wysiwyg

cd /srv/app

# método oficial de instalación del plugin
# https://strapi.io/documentation/developer-docs/latest/guides/registering-a-field-in-admin.html#introduction
strapi generate:plugin wysiwyg

cd plugins/wysiwyg

npm install @ckeditor/ckeditor5-react @ckeditor/ckeditor5-build-classic

echo 'Copying files...'
mkdir -p admin/src/components/MediaLib/
cp $PLUGIN_FILES_PATH/MediaLib-index.js admin/src/components/MediaLib/index.js

mkdir -p admin/src/components/Wysiwyg/
cp $PLUGIN_FILES_PATH/Wysiwyg-index.js admin/src/components/Wysiwyg/index.js

mkdir -p admin/src/components/CKEditor/
cp $PLUGIN_FILES_PATH/CKEditor-index.js admin/src/components/CKEditor/index.js

cp $PLUGIN_FILES_PATH/admin-index.js admin/src/index.js
echo 'Copy complete'

cd /srv/app

strapi build
fi

echo "Executing passed command (Dockerfile's CMD/compose's command)"
exec "$@"
